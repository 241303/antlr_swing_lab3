tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
}

prog    : (e+=expr | d+=decl | e+=block)* -> program(expr={$e}, decl={$d});

decl  :
        ^(VAR i1=ID) {locals.newSymbol($ID.text);} -> dek(n={$ID.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}


block : ^(BS {locals.enterScope();} (e+=expr | d+=decl | e+=block)* {locals.leaveScope();}) -> blockstart(expr={$e}, decls={$d})
      ;

expr    : ^(PLUS  e1=expr e2=expr) -> dodaj(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> subtract(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> multiply(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> divide(p1={$e1.st},p2={$e2.st})
        | ^(PODST i1=ID   e2=expr) {locals.hasSymbol($i1.text)}? -> substitute(key={$i1.text},value={$e2.st})
        | INT  {numer++;} -> int(i={$INT.text},j={numer.toString()})
        | ID {locals.hasSymbol($ID.text)}? -> read(key={$ID.text})
    ;
    